# minerva.js

Handles Sequelize models and helps you with migrations.

## Install

  npm install --save minerva.js

## Usage

  const Minerva = require('minerva.js');
  const minerva = new Minerva({
      dialect: 'postgres',
      username: 'user',
      password: 'pass',
      database: 'test',
      host: 'localhost',
      schema: 'test',
      schemaPrefix: 'production_'
  });

  minerva.sync()
  .then(() => console.log('models loaded and migrations upped.'));

  minerva.model.findAll(...);

This module loads the sequelize modes automatically (pattern is used by [express/sequelize example](https://github.com/sequelize/express-example)). You define the folder where your models are define. Optionally you can define migrations files (handled by the [umzug library](https://github.com/sequelize/umzug)).

| Field          | Default                                   | Description |
|----------------|-------------------------------------------|-------------|
| modelsDir      | `process.cwd() + '/db'`                   | The folder location where the sequelize models are. |
| dialect        | *none*                                    | The used sequelize dialect. |
| username       | *none*                                    | The user for the database connection. |
| password       | *none*                                    | The password for the database connection. |
| database       | *none*                                    | Name of the database. |
| host           | *none*                                    | Host of the database. |
| migrations     | `{ path: process.cwd() + '/migrations' }` | The options for the migrations (please check [the options of the umzug library](https://github.com/sequelize/umzug)). |
| schema         | *none*                                    | This define a schema for all models automatically. Please check the *schemas* section for more infos. |
| schemaPrefix   | *none*                                    | This adds a prefix to given schemas. Can be used for separating environments on the same database (e.g. *production_* and *staging_*). |
| storage        | `sequelize`                                    | The storage method used by the umzug library (for more infos check [umzug library](https://github.com/sequelize/umzug)). |
| storageOptions | `{}`                                    | Options for the storage method. |
| logging        | `false` | enabled logging for sequelize and umzug, but its disabled by default. If you use sequelize 4 use the [debug component](http://docs.sequelizejs.com/manual/tutorial/upgrade-to-v4.html#new-features) instead. |

## install missing dependencies

minerva.js does not includes sequelize. You have to install sequelize `npm i --save sequelize` separately.

## migration handling

If minerva finds migration files it will try to migrate automatically during the `sync`. It checks automatically which migrations are pending and will try to run them all. If one the migrations fail all pending migrations *since the sync* will be reverted.

If a schema is define and minerva is going to create the whole schema from scratch it will also add all migrations files automatically manually into the sequelize meta table (this works only if storage is set to `sequelize`). Otherwise it would start migrating, which would also fail. 
