const expect = require('chai').expect;
const minerva = require('../index');

minerva.setup({
    modelsDir: __dirname + '/db',
    dialect: 'postgres',
    username: 'user',
    password: 'pass',
    database: 'test',
    host: 'localhost',
    migrations: {
        path: __dirname + '/migrations',
    },
    schema: 'test'
});

describe('minerva.js', () => {

    it('should load propery', done => {
        expect(minerva.test).to.be.not.undefined;
        minerva.sync()
        .then(() => done());
    });

});
