module.exports = (sequelize, DataTypes) => {
    const test = sequelize.define('test', {
        id:    { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true, },
        value: { type: DataTypes.STRING },
    },
    {
        schema : sequelize.options.schema,
    });

    return test;
}
