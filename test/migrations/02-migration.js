module.exports = {
    up: (queryBuilder, Sequelize) => {
        return queryBuilder.addColumn({ tableName: 'tests', schema: 'test' }, 'test3', { type: Sequelize.STRING(16) });
    },

    down: function (queryBuilder, Sequelize) {
        return queryBuilder.removeColumn({ tableName: 'tests', schema: 'test' }, 'test3');
    }
};
