const fs = require('fs');
const Sequelize = require('sequelize');
const Umzug = require('umzug');
const path  = require('path');
const debug = require('debug')('minerva');

/**
* Minerva
*
* This class handles Sequelize models and migrations.
*/
class Minerva {

  /**
  * Sets the defaults.
  * @constructor
  */
  constructor() {
    this.config = {
      modelsDir: `${process.cwd()}/db`,
      logging: false,                     // its better to use DEBUG=sequelize:*
      storage: 'sequelize',
      storageOptions: {},
      migrations: {
        path: `${process.cwd()}/migrations`
      },
    }
  }

  /**
  * This sets the main configuration and loads the instances. The main configugration object is a combintation of Sequelizes config and
  * (http://docs.sequelizejs.com/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor) and the configuration
  * of the Umzug library (https://github.com/sequelize/umzug#configuration). So `logging` for example affets both libraries.
  * @constructor
  *
  * @param {object} config - The main configuration.
  * @param {string} [config.modelsDir="${process.cwd()}/db"] - The location of the Sequelize models.
  * @param {object} [config.storageOptions={}] - Options for umzug (https://github.com/sequelize/umzug#options-1)
  * @param {string} [config.logging=false] - Enables logging for Sequelize and migrations (its better to use `DEBUG=sequelize:*`)
  * @param {string} [config.migrations.path="${process.cwd()}/migrations"] - The location of the migarion files.
  * @param {string} [config.schema] - Sets a schema, which will be used for all models.
  * @param {string} [config.schemaPrefix] - A custom prefix, which will be added to the prefix.
  */
  setup(config = {}) {
    const models = {};

    config = Object.assign({}, this.config, config);
    config.storageOptions = Object.assign({}, this.config.storageOptions, config.storageOptions); // Object.assign does no deep copy
    config.migrations = Object.assign({}, this.config.migrations, config.migrations);

    this.config = config;

    // Some dbms doesn't support special characters inside the schema name, remove them
    // Important, do the changes before you initiate the sequelize object, otherwise its the wrong schema inside the models.
    if(this.config.schema) {
      this.config.schema = this.config.schema.replace(/[^a-zA-Z_]/g, '');

      if(this.config.schemaPrefix) {
        this.config.schema = this.config.schemaPrefix.replace(/[^a-zA-Z_]/g, '') + this.config.schema;
      }

      debug('using schema %s', this.config.schema);
    }

    this.Sequelize = Sequelize;
    this.sequelize = new Sequelize(this.config);

    // If umzug storage is sequelize, add the Sequelize instance and add parameters
    if(this.config.storage === 'sequelize') {
      this.config.storageOptions.sequelize = this.sequelize;
      this.config.migrations.params = [this.sequelize.getQueryInterface(), this.Sequelize]; // This will be passed to the acutal migration as params.

      // If a schema is added, have ot explictily add the schema to the storageOptions.
      if(this.config.schema) {
        this.config.storageOptions.schema = this.config.schema;
      }
    }
    this.umzug = new Umzug(this.config);

    if(!this.config.schema) {
      console.warn(`There is no schema defined. To get rid of this warning please define a schema in the configuration.`);
    }

    if(!fs.existsSync(this.config.modelsDir)) {
      throw Error(`Models directory ${this.config.modelsDir} cannot be found. Please check the configuration and set "modelsDir" correctly.`);
    }

    fs.readdirSync(this.config.modelsDir)
    .filter(file => file.endsWith('.js'))
    .forEach(file => {
      const model = this.sequelize['import'](path.join(this.config.modelsDir, file));
      const modelName = file.substr(0, file.indexOf('.'));

      if(this.config.schema && !model.options.schema) {
        throw Error(`You set a schema inside the config but its not configured inside the model "${modelName}"! Please add "schema: sequelize.options.schema" to the models options.`);
      }

      if(['sequelize', 'Sequelize', 'config'].indexOf(modelName) !== -1) {
        throw Error(`Name of the model ${modelName} is not allowed, please use a diffrenet one (not allowed 'Sequelize', 'sequelize', 'config')`);
      }

      models[modelName] = model;
      debug('model %s imported', modelName);
    });

    Object.keys(models).forEach(modelName => {
      if (models[modelName].associate) {
        models[modelName].associate(models);
      }
    });

    Object.assign(this, models); // add models to the main object

    debug('using config %O', this.config);
    return this;
  }

  /**
  * Syncs models with the database and runs migration if necessary. First it will check if the required schema is already there.
  * If the schema does not exist, it creates it, syncs the model with the db and adds the migrations files manually to the database.
  * If the schema exists it tries to execute the pending migrations. It gets the all pending migrations and keeps track of the executed ones. In the case that
  * the migrations fail Minerva will try to rollback the upped migrations.
  *
  * @return {Promise<null>} Resolves when everything is set up.
  */
  sync() {
    let schemaCreated = false; // Is used to determine if migartion should be executed or not (false means there is fresh install so no migraitons are necessary).

    debug('syncing with db %s using dialect %s...', this.config.host, this.config.dialect);

    return this.sequelize.showAllSchemas()
    .then(schemas => {
      if(this.config.schema && !schemas.includes(this.config.schema)) {  // Check if the schema is there, otherwise create it.
        debug('schema %s not found, creating it...', this.config.schema);

        schemaCreated = true;
        return this.sequelize.createSchema(this.config.schema);
      }
      return Promise.resolve();
    })
    .then(() => this.sequelize.sync())
    .then(() => {
      // If the migrations folder is not there, just skip migrations completely.
      if(!fs.existsSync(this.config.migrations.path)) {
        debug(`migrations directory ${this.config.migrations.path} was not found, skipping migrations`);
        return Promise.resolve();
      }

      // If the schema was created from scratch, then there are no migraitons necessary. But we have to add the migrations
      // to the database manually, otherwise it will try to migrate the next time when the app starts.
      if(schemaCreated) {
        debug('fresh schema was created, skipping migrations and adding entries manually');

        // Cannot cover other storage options like JSON, etc. Never used.
        if(this.config.storage !== 'sequelize') {
          console.warn(`Cannot add migrations for driver "${this.config.storage}" automatically, please add them manually.`);
          return Promise.resolve();
        }

        const migrations = fs.readdirSync(this.config.migrations.path);
        return this.umzug.storage.model.bulkCreate(migrations.map(file => { return { name: file } }))
        .then(migrations => {
          debug('added migrations manually %o', migrations.map(migration => migration.name));
          return Promise.resolve();
        });
      }

      debug('try running migrations...');
      return this.umzug.pending()
      .then(migrations => {
        const pendingMigrations = migrations.map(migration => migration.file); // important! migrations will be altered by the umzug lib, so copy elements!

        debug('pending migrations %o', pendingMigrations);

        // To catch errors properly a Promise is used. Its not fancy, I know, but otherweise I cannot
        // run other Promises in the catch-block. If the migrations fail I have to rollback (inside the catch-block).
        return new Promise((resolve, reject) => {
          this.umzug.up()
          .then(migrations => {
            debug('migration was sucessful. upped migrations %O', migrations);
            resolve();
          })
          .catch(error => {
            console.log('Migrations failed', error);

            debug('some migrations failed, rolling back...');
            this.umzug.executed()
            .then(migrations => {
              const executedMigrations = migrations.map(migration => migration.file);
              const alreadyUppedMigrations = executedMigrations.filter(migration => pendingMigrations.indexOf(migration) > -1);
              const failedMigrations = pendingMigrations.filter(migration => executedMigrations.indexOf(migration) === -1);

              debug('rolling back %o', alreadyUppedMigrations);
              debug('failed migrations %o', failedMigrations);
              return this.umzug.down(alreadyUppedMigrations);
            })
            .then(() => reject(error)) // if rollback successfull, migrations still failed. So reject.
            .catch(rollbackError => {  // if rollback also failed, just print the rollback error and reject the failed migration.
              console.error('Rollback failed', rollbackError);
              reject(error);
            });
          });
        });
      });
    });
  }
}

module.exports = new Minerva();
